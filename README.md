# Scraper #

Write a simple web scraper to get information about the boats and group them by charter company

### Task ###

* Check all Spanish boats on our stag server https://bt2stag.boataround.com/search?destinations=spain
* Get information about their services and extras and charter companies
* Summarize data for each charter company and their possible extras and services

### Bonus Task ###

* Try to find anomalies in prices and flag those in the final report

### Rules ###

* There are no rules in a world of web scraping :)
* OK, maybe just be careful with the staging server as it's not the most powerful machine :)

### Delivery ###

* ( Create git repository (github, bitbucket...) with your code <code>OR</code> pack your code to .zip ) AND send link or file to miro@boataround.com and to pavel@boataround.com
